using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class atac : MonoBehaviour
{
    private Animator _anim;
    void Start()
    {
        _anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            _anim.SetBool("atacar", true);
        }
        
    }
    public void dejarAtacat()
    {
        _anim.SetBool("atacar", false);
    }
}
