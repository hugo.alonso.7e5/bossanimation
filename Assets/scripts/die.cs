using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class die : MonoBehaviour
{
    private Animator _anim;
    // Start is called before the first frame update
    void Start()
    {
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.G))
        {
            _anim.SetBool("morir",true);
        }
        if (Input.GetKey(KeyCode.F))
        {
            _anim.SetBool("morir", false);
        }

    }
}
