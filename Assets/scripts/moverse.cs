using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverse : MonoBehaviour
{
    private Animator _anim;
    void Start()
    {
        _anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        transform.localScale = new Vector3(0.5f,0.5f,1f);
        float movimientoH = Input.GetAxisRaw("Horizontal");
        _anim.SetBool("moverse", movimientoH != 0);

        //if (Input.GetKey(KeyCode.D))
        //{

        //    _anim.SetBool("moverse", true);
        //}

    }
}
